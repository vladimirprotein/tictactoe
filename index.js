var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var players = {};
var games = {};

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

var port = process.env.PORT || 3002;

http.listen(port, function(){
  console.log('listening on *:3002');
});

io.on('connection', (socket)=>{
	socket.on('new player', (obj, callback)=>{
		obj.name = obj.name.toLowerCase().trim();
		if(socket.username){
			callback('You already have a username: '+ socket.username);
			return;
		}
		if(obj.name == ''){
			callback('Where have you seen an empty username?');
			return;
		}
		if(obj.name in players || obj.name == 'bot'){
			callback('Be Unique.. This one already exists');
			return;
		}
		socket.username = obj.name;
		players[obj.name] = socket;
		console.log(socket.username+' is in the game...');
		callback('success');
		io.emit('players online', Object.keys(players));
	});

	socket.on('disconnect', ()=>{
		if(socket.username in players){
			console.log(socket.username + ' disconnected...');
			if(players[socket.opponent]){
				players[socket.opponent].emit('opponent left');
				delete players[socket.opponent].opponent;
				delete players[socket.opponent].gameID;
			} 
			if(games[socket.gameID]) delete games[socket.gameID];
			delete players[socket.username];
			io.emit('players online', Object.keys(players));
		}
	});

	socket.on('game request', (obj, callback)=>{
		if(!(socket.username in players)){
			callback('Get a username first..');
			return;
		}
		if(!(obj.name in players) && obj.name != 'bot'){
			callback('This user does not exist!');
			return;
		}
		if(obj.name == socket.username){
			callback('Want to play with yourself??');
			return;
		}
		if(players[socket.username].opponent){
			callback('You are in a game right now.');
			return;
		}
		if(players[obj.name]) if(players[obj.name].opponent){
			callback('User in a game right now.');
			return;
		}

		if(obj.name != 'bot'){
			soc = players[obj.name];
			soc.emit('game request rec', {name: socket.username});
			callback('Game request sent to ' + obj.name);
			console.log(socket.username + ' is playing with ' + obj.name);
			socket.emit('game request acc', {name: obj.name});

			gameID = Math.random()*1000000;

			players[socket.username].gameID = gameID;
			players[obj.name].gameID = gameID;

			players[socket.username].opponent = obj.name;
			players[obj.name].opponent = socket.username;

			players[socket.username].symbol = 'X';
			players[obj.name].symbol = 'O';

			players[socket.username].move = false;
			players[obj.name].move = true;


			games[gameID] = {p1: socket.username, p2: obj.name, winner: false, blockedsq: [] };
			games[gameID][obj.name] = [0,0,0,0,0,0,0,0];
			games[gameID][socket.username] = [0,0,0,0,0,0,0,0];
			games[gameID]['overall'] = [0,0,0,0,0,0,0,0];

			console.log(games[gameID]);
		}
		else{ //bot algorithm..
			callback('Game started with Ms. bot..');
			console.log(socket.username+ ' is playing with bot');
			socket.emit('bot game started', {});

			gameID = Math.random()*1000000;
			socket.gameID = gameID;
			socket.opponent = 'bot';
			socket.symbol = 'O';
			socket.move = true;

			games[gameID] = {p1: socket.username, p2: 'bot', winner: false, blockedsq: [] };
			games[gameID][socket.username] = [0,0,0,0,0,0,0,0];
			games[gameID]['bot'] = [0,0,0,0,0,0,0,0];
			games[gameID]['overall'] = [0,0,0,0,0,0,0,0];

			console.log(games[gameID]);
		}

	});

	socket.on('move made', (obj, callback)=>{
		if(!(socket.username in players)){
			callback('Get a username first..');
			return;
		}
		if(!socket.gameID){
			callback('Not in a game');
			return;
		}
		if(!players[socket.opponent] && socket.opponent != 'bot'){
			callback('Opponent left');
			return;
		}
		if(socket.move == false){
			return;
		}
		if(games[socket.gameID]['blockedsq'].includes(obj.cell)){
			return;
		}
		if(games[socket.gameID]['winner'] != false){
			return;
		}

		if(socket.opponent != 'bot'){
			var opp_socket = players[socket.opponent];
			var symbol = socket.symbol;
			games[socket.gameID]['blockedsq'].push(obj.cell);
			socket.emit('move made', {cell: obj.cell, symbol: symbol, move: false});
			opp_socket.emit('move made', {cell: obj.cell, symbol: symbol, move:true});
			socket.move = false;
			opp_socket.move = true;

			console.log(games[socket.gameID]);

			// Game winner checking algorithm..
			var i1 = parseInt(obj.cell.charAt(0));
			var i2 = parseInt(obj.cell.charAt(1));
			games[socket.gameID][socket.username][i1]++;
			games[socket.gameID][socket.username][i2+3]++;
			games[socket.gameID]['overall'][i1]++;
			games[socket.gameID]['overall'][i2+3]++;
			if(i1 == i2){
				games[socket.gameID][socket.username][6]++;
				games[socket.gameID]['overall'][6]++;
			}
			if(i1+i2 == 2){
				games[socket.gameID][socket.username][7]++;
				games[socket.gameID]['overall'][7]++;
			}
			console.log(games[socket.gameID]);

			if(games[socket.gameID][socket.username].includes(3)){
				games[socket.gameID]['winner'] = socket.username;
				var winline = games[socket.gameID][socket.username].indexOf(3);
				socket.emit('game over', {winner: true, line: winline});
				opp_socket.emit('game over', {winner: false, line: winline});
				delete games[socket.gameID];
				delete players[socket.opponent].opponent;
				delete players[socket.opponent].gameID;
				delete socket.opponent;
				delete socket.gameID;
			}
			else if(games[socket.gameID]['blockedsq'].length == 9){
				socket.emit('game over', {winner: 'draw'});
				opp_socket.emit('game over', {winner: 'draw'});
				delete games[socket.gameID];
				delete players[socket.opponent].opponent;
				delete players[socket.opponent].gameID;
				delete socket.opponent;
				delete socket.gameID;
			}
		}
		else{ // bot algorithm..
			var symbol = socket.symbol;
			console.log('cell:'+obj.cell);
			games[socket.gameID]['blockedsq'].push(obj.cell);
			console.log(games[socket.gameID]['blockedsq']);
			socket.emit('move made', {cell: obj.cell, symbol: symbol, move: false});
			socket.move = false;

			// checking if user won..
			var i1 = parseInt(obj.cell.charAt(0));
			var i2 = parseInt(obj.cell.charAt(1));
			games[socket.gameID][socket.username][i1]++;
			games[socket.gameID][socket.username][i2+3]++;
			games[socket.gameID]['overall'][i1]++;
			games[socket.gameID]['overall'][i2+3]++;
			if(i1 == i2){
				games[socket.gameID][socket.username][6]++;
				games[socket.gameID]['overall'][6]++;
			}
			if(i1+i2 == 2){
				games[socket.gameID][socket.username][7]++;
				games[socket.gameID]['overall'][7]++;
			}

			console.log(games[socket.gameID]);

			if(games[socket.gameID][socket.username].includes(3)){
				var winline = games[socket.gameID][socket.username].indexOf(3);
				games[socket.gameID]['winner'] = socket.username;
				socket.emit('game over', {winner: true, line: winline});
				delete games[socket.gameID];
				delete socket.opponent;
				delete socket.gameID;
			}
			else if(games[socket.gameID]['blockedsq'].length == 9){
				socket.emit('game over', {winner: 'draw'});
				delete games[socket.gameID];
				delete socket.opponent;
				delete socket.gameID;
			}

			//bot move..
			function botmove(sqr){
				console.log('bot move: '+sqr);
				games[socket.gameID]['blockedsq'].push(sqr);
				console.log(games[socket.gameID]['blockedsq']);
				socket.emit('move made', {cell: sqr, symbol: 'X', move: true});
				socket.move = true;

				var i1 = parseInt(sqr.charAt(0));
				var i2 = parseInt(sqr.charAt(1));
				games[socket.gameID]['bot'][i1]++;
				games[socket.gameID]['bot'][i2+3]++;
				games[socket.gameID]['overall'][i1]++;
				games[socket.gameID]['overall'][i2+3]++;
				if(i1 == i2){
					games[socket.gameID]['bot'][6]++;
					games[socket.gameID]['overall'][6]++;
				}
				if(i1+i2 == 2){
					games[socket.gameID]['bot'][7]++;
					games[socket.gameID]['overall'][7]++;
				}

				if(socket.gameID) if(games[socket.gameID]){console.log(games[socket.gameID]);}

				if(games[socket.gameID]['bot'].includes(3)){
					var winline = games[socket.gameID]['bot'].indexOf(3);
					games[socket.gameID]['winner'] = 'bot';
					socket.emit('game over', {winner: false, line: winline});
					delete games[socket.gameID];
					delete socket.opponent;
					delete socket.gameID;
				}
				else if(games[socket.gameID]['blockedsq'].length == 9){
					socket.emit('game over', {winner: 'draw'});
					delete games[socket.gameID];
					delete socket.opponent;
					delete socket.gameID;
				}
				 
			}

			// decision of a bot move..
			if(socket.gameID) if( (games[socket.gameID]['winner'] == false)  &&  (games[socket.gameID]['blockedsq'].length != 9)  ){
				
				// marking the middle square if it's empty
				if(!games[socket.gameID]['blockedsq'].includes('11')){
					botmove('11'); console.log('caught1'); return;
				}
				if(games[socket.gameID]['blockedsq'].length == 3){
					if( (games[socket.gameID]['overall'][6] == 3) || (games[socket.gameID]['overall'][7] == 3) ){
						if(games[socket.gameID]['blockedsq'][1] == '11'){
							botmove('12'); console.log('caught2'); return;
						}
						else{
							botmove('20'); console.log('caught3'); return;
						}
						
					}
				}
				


				// seeing if win is possible in this move..
				for(var i = 0; i<8 ; i++){
					if((games[socket.gameID]['bot'][i] == 2) && (games[socket.gameID]['overall'][i] == 2)){
						if(i>=0 && i<=2){
							for(var j=0; j<=2; j++){
								if(!games[socket.gameID]['blockedsq'].includes(i.toString()+j.toString())){
									botmove(i.toString()+j.toString()); console.log('caught4');  return;
								}
							}
						}
						else if(i>=3 && i<=5){
							i = i-3;
							for(var j=0; j<=2; j++){
								if(!games[socket.gameID]['blockedsq'].includes(j.toString()+i.toString())){
									botmove(j.toString()+i.toString()); console.log('caught5'); return;
								}
							}
						}
						else if(i==6){
							if(!games[socket.gameID]['blockedsq'].includes('00')){
								botmove('00'); return;
							}
							if(!games[socket.gameID]['blockedsq'].includes('11')){
								botmove('11'); return;
							}
							if(!games[socket.gameID]['blockedsq'].includes('22')){
								botmove('22'); return;
							}
						}
						else{
							if(!games[socket.gameID]['blockedsq'].includes('20')){
								botmove('20'); return;
							}
							if(!games[socket.gameID]['blockedsq'].includes('11')){
								botmove('11'); return;
							}
							if(!games[socket.gameID]['blockedsq'].includes('02')){
								botmove('02'); return;
							}
						}
					}
				}


				// obstruction of the 'win in next move' for opponent..
				for(var i = 0; i<8 ; i++){
					if((games[socket.gameID][socket.username][i] == 2) && (games[socket.gameID]['overall'][i] == 2)){
						if(i>=0 && i<=2){
							for(var j=0; j<=2; j++){
								if(!games[socket.gameID]['blockedsq'].includes(i.toString()+j.toString())){
									botmove(i.toString()+j.toString()); console.log('caught6'); return;
								}
							}
						}
						else if(i>=3 && i<=5){
							i = i-3;
							for(var j=0; j<=2; j++){
								if(!games[socket.gameID]['blockedsq'].includes(j.toString()+i.toString())){
									botmove(j.toString()+i.toString()); console.log('caught7'); return;
								}
							}
						}
						else if(i==6){
							if(!games[socket.gameID]['blockedsq'].includes('00')){
								botmove('00'); return;
							}
							if(!games[socket.gameID]['blockedsq'].includes('11')){
								botmove('11'); return;
							}
							if(!games[socket.gameID]['blockedsq'].includes('22')){
								botmove('22'); return;
							}
						}
						else{
							if(!games[socket.gameID]['blockedsq'].includes('20')){
								botmove('20'); return;
							}
							if(!games[socket.gameID]['blockedsq'].includes('11')){
								botmove('11'); return;
							}
							if(!games[socket.gameID]['blockedsq'].includes('02')){
								botmove('02'); return;
							}
						}
					}
				}


				// potential trap detection..
				for(var i=0; i<3; i++){
					for(var j=0; j<3; j++){
						var array1 = games[socket.gameID][socket.username];
						var array2 = games[socket.gameID]['overall'];
						if(!games[socket.gameID]['blockedsq'].includes(i.toString()+j.toString())){
							array1[i]++;
							array1[j+3]++;
							array2[i]++;
							array2[j+3]++;
							if(i == j){
								array1[6]++;
								array2[6]++;
							}
							if(i+j == 2){
								array1[7]++;
								array2[7]++;
							}

							var count = 0;
							for(var z=0; z<8; z++){
								if( (array1[z]==2) && (array2[z]==2) ){
									count++;
								}
							}
							array1[i]--;
							array1[j+3]--;
							array2[i]--;
							array2[j+3]--;
							if(i == j){
								array1[6]--;
								array2[6]--;
							}
							if(i+j == 2){
								array1[7]--;
								array2[7]--;
							}

							if(count>1){
								botmove(i.toString()+j.toString()); console.log('caught8'); return;
							}
						}
					}
				}


				// looking for making 2 mark in a possible winning row..
				for(var e = 0; e<8 ; e++){
					if((games[socket.gameID]['bot'][e] == 1) && (games[socket.gameID]['overall'][e] == 1)){
						if(e>=0 && e<=2){
							for(var f=0; f<=2; f++){
								if(!games[socket.gameID]['blockedsq'].includes(e.toString()+f.toString())){
									botmove(e.toString()+f.toString()); console.log('caught9'); return;
								}
							}
						}
						else if(e>=3 && f<=5){
							e = e-3;
							for(var f=0; f<=2; f++){
								if(!games[socket.gameID]['blockedsq'].includes(f.toString()+e.toString())){
									botmove(f.toString()+e.toString()); console.log('caught10'); return;
								}
							}
						}
						else if(e==6){
							if(!games[socket.gameID]['blockedsq'.includes('00')]){
								botmove('00'); return;
							}
							if(!games[socket.gameID]['blockedsq'.includes('11')]){
								botmove('11'); return;
							}
							if(!games[socket.gameID]['blockedsq'.includes('22')]){
								botmove('22'); return;
							}
						}
						else{
							if(!games[socket.gameID]['blockedsq'.includes('20')]){
								botmove('20'); return;
							}
							if(!games[socket.gameID]['blockedsq'.includes('11')]){
								botmove('11'); return;
							}
							if(!games[socket.gameID]['blockedsq'.includes('02')]){
								botmove('02'); return;
							}
						}
					}
				}


				// random move..
				for(var i=0; i<3; i++){
					for(var j=0; j<3; j++){
						if(!games[socket.gameID]['blockedsq'].includes(i.toString()+j.toString())){
							botmove(i.toString()+j.toString()); console.log('caught11'); return;
						}
					}
				}

				

			}//
		}
	});
});